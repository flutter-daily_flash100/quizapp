import 'package:flutter/material.dart';
import 'package:flutter_application_1/first.dart';
import 'package:flutter_application_1/login.dart';
import 'package:flutter_application_1/new.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: DreamCompany(),
      debugShowCheckedModeBanner: false,
    );
  }
}
//
